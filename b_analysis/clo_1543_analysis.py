'''
Analysis for CLO1543

Selection from PGO data: soortlijst, soort, periode
Spatial selection: 250m hokken with > 0 m2 of one or more beheertypes

output: area (ha) with toename, afname, stabiel

Hans Roelofsen, september 2019

'''

import os
import pandas as pd
import datetime

from z_utils import clo

'''
Model run identifier
'''
identifier = 'HD06'

'''
Query PGO data for a specific SNL soortenlijst and specific spatial filter  
'''
soortgroepen = ['vogel', 'vlinder', 'vaatplant']

snl_soortlijst = ['HalfnatuurlijkGrasland']  # soortenlijst behorende bij een SNL type
sub_soortlijst = ['EcoSysLijst']  # SNL, Bijlage 1 of EcoSysLijst?

snl_gebieden_wel = ['N1202', 'N1301']  # 250m cellen met welk SNL type?
snl_gebied_drempelwaarde = 0
snl_gebieden_niet = ['N1900']

# Definieer de numerieke range voor de categorieren Afname, Stabiel, Toename:
# [a, b, c, d]
# a <= Afname < b
# b <= Stabiel < c
# c <= Toename < d
dif_cats = [-1000, -1, 2, 1000]  # see https://docs.scipy.org/doc/numpy/reference/generated/numpy.histogram.html
dif_labs = ['afname', 'stabiel', 'toename']  # must be in INCREASING order

# choose one of ['1994-2001', '2002-2009', '2010-2017'] for periode A and periode B, where stats will be made A-B
periodes = ['1994-2001', '2002-2009', '2010-2017']
periode_A = '2010-2017'
periode_B = '2002-2009'

'''
SPATIAL SELECTION 
'''
hokken, spat_query = clo.query_250m_hokken(snl_gebieden_wel, snl_gebied_drempelwaarde, snl_gebieden_niet)

'''
SELECTION FROM PGO DATA
'''
pgo_query = ["soortlijst in {0} & snl in {1} & soortgroep in {2}".format(sub_soortlijst, snl_soortlijst, soortgroepen)]
pgo_dat = clo.query_pgo_store(pgo_query)

'''
Pivot to show plant, vogel, vlinder count per hok. Colums are soortgroep, then periode. Assume always 1 soortlijst only! 
'''
dat_piv = pd.pivot_table(pgo_dat, index='hok_id', columns=['soortgroep', 'periode'], values='n', aggfunc='sum')
print('PGO data are based in {0} 250m hokken'.format(dat_piv.shape[0]))

'''Calc CLO1543 between periode A - periode B for plant, vogel, vlinder'''
idx = pd.IndexSlice
plant_trend, plant_score = clo.calc_clo_1543(s_periode_A=dat_piv.loc[:, idx['vaatplant', periode_A]],
                                             s_periode_B=dat_piv.loc[:, idx['vaatplant', periode_B]],
                                             bins=dif_cats, labels=dif_labs, species='vaatplant')
vogel_trend, vogel_score = clo.calc_clo_1543(s_periode_A=dat_piv.loc[:, idx['vogel', periode_A]],
                                             s_periode_B=dat_piv.loc[:, idx['vogel', periode_B]],
                                             bins=dif_cats, labels=dif_labs, species='vogel')
vlinder_trend, vlinder_score = clo.calc_clo_1543(s_periode_A=dat_piv.loc[:, idx['vlinder', periode_A]],
                                                 s_periode_B=dat_piv.loc[:, idx['vlinder', periode_B]],
                                                 bins=dif_cats, labels=dif_labs, species='vlinder')

'''Create new df containing counts-per-periode, scores and trends for all soortgroepen. Then attach to hokken.'''
flat_cols = ['_'.join(tuple_col) for tuple_col in list(dat_piv)]
dat_piv.columns = flat_cols
foo_df = pd.concat([dat_piv, plant_trend, plant_score, vogel_trend, vogel_score, vlinder_trend, vlinder_score], axis=1)
# note observations are here reduced to valid hokken only, how=left!
clo_df = pd.merge(left=hokken, right=foo_df, how='left', right_index=True, left_index=True)

'''Niet alle hokken zullen een PGO observatie hebben. Vul score kolommen in met `onbekend`.'''
clo_df.fillna(value={'score_vaatplant': 'onbekend', 'score_vogel': 'onbekend', 'score_vlinder': 'onbekend',
                     'trend_vaatplant': 'onbekend', 'trend_vogel': 'onbekend', 'trend_vlinder': 'onbekend'},
              inplace=True)

'''
Bereken totaal areaal per soortgroep voor CLO scores Toename, Stabiel, Afname, Onbekend.
Bereken ook netto toename (Toename-Afname) en score als netto/totaal areal
'''
plant_out = pd.pivot_table(clo_df, index='score_vaatplant', values='areaal_ha', aggfunc='sum').T \
              .rename({'areaal_ha': 'vaatplant'}, axis=0)

vogel_out = pd.pivot_table(clo_df, index='score_vogel', values='areaal_ha', aggfunc='sum').T \
              .rename({'areaal_ha': 'vogel'}, axis=0)

vlinder_out = pd.pivot_table(clo_df, index='score_vlinder', values='areaal_ha', aggfunc='sum').T \
                .rename({'areaal_ha': 'vlinder'}, axis=0)

# concatenate into single dataframe. Note: index = [vaatplant, vogel, vlinder], cols = [afname, onbekend, stabiel,
# toename, netto, score, tot_areaal_ha]
clo_out = pd.concat([plant_out, vogel_out, vlinder_out])
clo_out.loc[:, 'netto'] = clo_out.apply(lambda row: row.toename - row.afname, axis=1)
clo_out.loc[:, 'score'] = clo_out.apply(lambda row: row.netto / hokken.areaal_ha.sum() * 100, axis=1)
clo_out.loc[:, 'tot_areaal_ha'] = clo_out.apply(lambda row: row.toename + row.afname + row.stabiel + row.onbekend,
                                                axis=1)
# TODO: ignore NAS in sommatie

'''
Write report
'''
timestamp = datetime.datetime.now().strftime("%y%m%d-%H%M")
out_base_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\CLO1543'
out_dir = os.path.join(out_base_dir, 't{0}'.format(timestamp))
os.mkdir(out_dir)

basename = 'clo1543_{0}_{1}'.format(identifier, timestamp)
pgo_dat_summary = pd.pivot_table(pgo_dat, index='soortgroep', columns=['snl', 'periode'], values='n', aggfunc='count')
cat_lims = [(dif_cats[i-1], dif_cats[i]) for i in range(1, len(dif_cats))]
clo_scores = ['{0} <= {1} < {2}'.format(l, x, u) for x, (l, u) in zip(dif_labs, cat_lims)]
header = '#Model run dated: {0} by {1}\n#\n' \
         '#Extract from PGO species distribution data with PGO Query:\n' \
         '#  Soortgroepen: {2}\n' \
         '#  SNL Soortlijst: {3}\n' \
         '#  SNL Sub-soortlijst: {4}\n' \
         '#  ==> {5} observations in {6} different 250m hokken (see also PGO DATA SUMMARY)\n' \
         '#Selection from 250m hokken grid with Beheertypen Query:\n' \
         '#  {7}\n' \
         '#  ==> {8} 250m hokken with total {9} hectare.\n' \
         '#Trend refers to species difference between {10}-{11}\n' \
         '#Scores are as follows: {12}\n' \
         .format(timestamp, os.environ.get('USERNAME'), ', '.join([soort for soort in soortgroepen]),
                 ', '.join(snl for snl in snl_soortlijst), '-'.join(sub for sub in sub_soortlijst),
                 pgo_dat.shape[0], dat_piv.shape[0], spat_query, hokken.shape[0], hokken.areaal_ha.sum(),
                 periode_A, periode_B, clo_scores)

footer = '\nMade with Python 3.5 using pandas, geopandas, by Hans Roelofsen, WEnR team B&B, dated {0}'.format(timestamp)

with open(os.path.join(out_dir, basename + '.txt'), 'w') as f:
    f.write(header)
    f.write('\n###PGO DATA SUMMARY###\n')
    f.write(pgo_dat_summary.to_csv(sep='\t', line_terminator='\r'))
    f.write('\n##### HECTARE-TOENAME-STABIEL-AFNAME #####\n')
    f.write(clo_out.to_csv(sep='\t', line_terminator='\r'))
    f.write('\n###DATA per HOK###\n')
    f.write(clo_df.to_csv(sep='\t', line_terminator='\r'))
    f.write('\n\n')
    f.write(footer)

# Write selected hokken to shapefile
clo_df.index = clo_df.index.rename('dummy')
clo_gdf = clo.df_2_gdf(clo_df)
clo_gdf.to_file(os.path.join(out_dir,  '{0}.shp'.format(basename)))


