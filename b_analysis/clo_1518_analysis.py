'''
Python script for CLO indicator 1518 - Areaal Ecosysteemkwaliteit
(https://www.clo.nl/indicatoren/nl1518-areaal-ecosysteemkwaliteit)

==SOURCE DATA==
Firstly: 'kansenkaarten' van de 3 PGOs (FLORON, SOVON, Vlinderstichting). Zie [1] voor PGO data. De PGO data
is georganiseerd in 1.456.000 (1.120 cols, 1.300 rows) 250m hokken met daarin het aantal soorten per hok gegeven:
= een soortenlijst behorende bij een SNL beheertype (https://www.bij12.nl/onderwerpen/natuur-en-landschap/index-natuur-en-landschap/)
= sub-soortenlijst (SNL, Bijlage 1 of VHR)
= tijdsperiode (1994-2001, 2002-2009, 2010-2017)
Soortaantallen kunnen 0 of > 0 zijn, of NoData.

De originele kansenkaarten staan hier:
  \\wur\dfs-root\PROJECTS\hotspots_pgos\a_Source_Data\Vaatplanten\Soortenrijkdom\
  \\wur\dfs-root\PROJECTS\hotspots_pgos\a_Source_Data\vlinders\
  \\wur\dfs-root\PROJECTS\hotspots_pgos\a_Source_Data\vogels\Soortenrijkdom\
Alle kansenkaarten zijn omgezet naar een tabel in CSV:
  \\wur\dfs-root\PROJECTS\hotspots_pgos\b_Prelim_Results\compiled_data_hans\broedvogels_csv_format\
  \\wur\dfs-root\PROJECTS\hotspots_pgos\b_Prelim_Results\compiled_data_hans\vaatplant_csv_format\
  \\wur\dfs-root\PROJECTS\hotspots_pgos\b_Prelim_Results\compiled_data_hans\vlinder_csv\
Alle CSV tabellen zijn gecombineerd in een HD5 database:
  \\wur\dfs-root\PROJECTS\hotspots_pgos\b_Prelim_Results\compiled_data_hans\HD5\pgo_dat_191017-1323.h5

Secondly: een shapefile met gegevens van alle 1.456.000 250m hokken. Voor elk hok is bekend:
= het areaal in m2 van elk van de SNL beheertypen (1 hok kan > 1 Beheertype bevatten)
= areaal van elk van de Provincies (nooit meer dan 1 provincie per hok. 'Geen' voor hokken in de Noordzee etc
= indicatie of het hok wel/niet in Twente ligt

==PROCESSING==
Voor de PGO data, maak een selectie naar:
  soortgroep
  soortlijst (ie soortlijst behorende bij een SNL BeheerType)
  sub-soortlijst (ie, de SNL lijst, Bijlage 1 of VHR)
  tijdsperiode
Voor de hokken, maar een selectie naar:
  Beheertype(n) areaal >= X sq meter
  Beheertype(n) areaaal == 0 sq meter
  Provincie(n) areaal >= X sq meter
In principe kun je > 1 SNL BT selecteren, maar het handigst is om slechts 1 BeheerType mee te nemen in de query

De PGO data wordt 'gedraaid' (pivot table) op de unieke 250m hokken. Het aantal soorten wordt geteld per hok,
uitgesplitst naar: soortgroep
                     sub-soortlijst
                       periode

Vervolgens worden per hok aanvullende kolommen toegevoegd (kolomnaam):
  totaal aantal Bijlage 1 soorten per periode (*periode*_Bijl1_tot)
  totaal aantal Bijlage 1 soorten per periode met max 2 (*periode*_Bijl1_cap)
  totaal aantal SNL soorten per periode (*periode*_SNL_tot)
  som van *periode*_Bijl1_cap, *periode*_SNL_tot (*periode*_SNL-Bijl1_tot)
  de habitatkwaliteitscore (Laag|VrijLaag|VrijHoog|Hoog) voor een geselecteerde periode (*periode*_CLO1518_score)
  de geselecteerde SNL Beheertypes (gebaseerd op de 250m hokken selectie) (snl_bt)
  het ecosysteemtype correspondeert snl_bt (ecosystype)
  identificatiecode van het hok (<X-coordinaat>_<Y-coordinaat), refererend aan de RD-New coordinaten van de LINKER
   BOVENHOEK van elk hok (hok_id)
  het areaal in hectare van de snl_bt (areaal_ha)
  de naam van de Provincie waarin het hok valt (provincie)

==OUTPUT==
Een rapport in *.txt met beschriving van de PGO en Spatial Queries en de draai-tabel

Hans Roelofsen, 16 october 2019

'''

import os
import pandas as pd
import datetime
import numpy as np
import argparse

from z_utils import clo

'''
Parse arguments
'''
parser = argparse.ArgumentParser()
parser.add_argument('--soortgroepen', nargs='+', type=str)
parser.add_argument('--snl_sp_lst', help='SNL BT species list')
parser.add_argument('--sub_sp_lst', help='Sub species list', nargs='+', type=str)
parser.add_argument('--hok_snl_wel', help='Hokken with SNL BT type > 0 m2')
parser.add_argument('--hok_snl_niet', help='Hokken with SNL BT type == 0 m2')
args = parser.parse_args()

'''
#Manual arguments for testing
soortgroepen = ['vaatplant', 'vogel', 'vlinder']
snl_soortlijst = ['N1206']
sub_soortlijst = ['SNL', 'Bijl1']
snl_gebieden_wel = ['N1206']
snl_gebieden_niet = []
'''

'''
Parse arguments and convert to lists if not already   
'''
soortgroepen = args.soortgroepen if isinstance(args.soortgroepen, list) else [args.soortgroepen]  # soortgroepen include

# Welke SNL soortlijst?
snl_soortlijst = [args.snl_sp_lst]  # soortenlijst behorende bij een SNL type
sub_soortlijst = args.sub_sp_lst if isinstance(args.sub_sp_lst, list) else [args.sub_sp_lst]

# Selection of 250m cells to include in the analysis
snl_gebieden_wel = args.hok_snl_wel if isinstance(args.hok_snl_wel, list) else [args.hok_snl_wel]  # 250m cellen met welk SNL type?
snl_gebieden_niet = []#args.hok_snl_niet if isinstance(args.hok_snl_niet, list) else [args.hok_snl_niet]  # 250m cellen met welk SNL type?

# Reporting periode and quality indicator labels for CLO 1518
clo_labels = ['Laag', 'Vrij Laag', 'Vrij Hoog', 'Hoog']

'''
SPATIAL SELECTION 
'''
hokken, spat_query = clo.query_250m_hokken(snl_gebieden_wel, snl_gebieden_niet)
hokken_cols = pd.MultiIndex.from_product([["_"], ["_"], hokken.columns.to_list()],
                                         names=['First', 'Second', 'Third'])
hokken.columns = hokken_cols

'''
SELECTION FROM PGO DATA
'''
pgo_query = ["soortlijst in {0} & snl in {1} & soortgroep in {2}".format(sub_soortlijst, snl_soortlijst, soortgroepen)]
pgo_dat = clo.query_pgo_store(pgo_query)

'''
Pivot PGO data to show species count per periode, soortgroep, soortlijst 
'''
dat_piv = pd.pivot_table(data=pgo_dat, index='hok_id', columns=['periode', 'soortgroep', 'soortlijst'],
                         values='n', aggfunc='sum', dropna=False)
# Vanaf hier multi-index kolommen.

'''
merge PGO observation data in dat_piv with info on the hokken from hokken. 3-Levle columns are maintained!
'''
idx = pd.IndexSlice
dat_snl = pd.merge(left=dat_piv, right=hokken.loc[:, idx['_', '_', ['hok_id', 'areaal_ha', 'provincie', 'twente_hdr']]],
                   left_index=True, right_index=True, how='right')  # how = right, dus bewaar alle hokken!
dat_snl.index.rename('hok_id', inplace=True)

'''
Calculate metrics for each cell: total Bijlage 1, Bijlage 1 capped to max 2, total SNL species, total SNL + Bijl1-cap
'''
# Add Bijlage 1 totals and capped to 2 max per periode as new columns
for periode in list(set(pgo_dat.periode)):
    try:
        bijl1_only = dat_snl.loc[:, idx[periode, :, 'Bijl1']]
        bijl1_tot = bijl1_only.sum(axis=1)  # sum per over rows, i.e. over species-groups
        bijl1_cap = pd.Series(np.where(bijl1_tot > 2, 2, bijl1_tot), index=bijl1_tot.index)  # if > 2, then 2
        dat_snl[(periode, 'Bijl1', 'tot')] = bijl1_tot  # add as new columns under the periode
        dat_snl[(periode, 'Bijl1', 'cap')] = bijl1_cap
    except KeyError:  # not all periods may be present
        continue

# Total number of SNL species per periode as new columns
for periode in list(set(pgo_dat.periode)):
    try:
        periode_dat = dat_snl.loc[:, idx[periode, :, 'SNL']]  # select SNL and Bijlage1-tot columns
        snl_total_species = periode_dat.sum(axis=1)  # sum over each row of selected columns
        dat_snl[(periode, 'SNL', 'tot')] = snl_total_species
    except KeyError:
        continue

# Add total species per hok as SNL-tot + Bijl1-cap as new columns
for periode in list(set(pgo_dat.periode)):
    try:
        periode_dat = dat_snl.loc[:, [(periode, 'SNL', 'tot'), (periode, 'Bijl1', 'cap')]]
        tot_species = periode_dat.sum(axis=1)
        dat_snl[(periode, 'SNL-Bijl1', 'tot')] = tot_species
    except KeyError:
        continue

'''
Calculate CLO1518 quality score per hok, i.e. total nr of species in *periode* relative to hok with most species
'''
clo1518_dat = dat_snl.loc[dat_snl.loc[:, idx['_', '_', 'twente_hdr']] == 0, :]  # do not use Twente hokken
clo1518_1994_2001_score, clo1518_1994_2001_report = clo.calc_clo_1518(clo1518_dat, '1994-2001', clo_labels)
clo1518_2002_2009_score, clo1518_2002_2009_report = clo.calc_clo_1518(clo1518_dat, '2002-2009', clo_labels)
clo1518_2010_2017_score, clo1518_2010_2017_report = clo.calc_clo_1518(clo1518_dat, '2010-2017', clo_labels)
# from here on the multi-index columns are lost, but that's ok as wer're nearly done anyway
dat_snl = dat_snl.join(other=clo1518_1994_2001_score, how='left')
dat_snl = dat_snl.join(other=clo1518_2002_2009_score, how='left')
dat_snl = dat_snl.join(other=clo1518_2010_2017_score, how='left')

'''
Wrap up
'''
# rename columns
new_colnames = ['_'.join([x for x in colname if x is not '_']) for colname in dat_snl.columns.tolist() if
                isinstance(colname, tuple)]
dat_snl.rename(columns=dict(zip(dat_snl.columns.tolist(), new_colnames)), inplace=True)

'''Label each hok in dat_snl with: 1) SNL BeheerType (NXXYY), 2) EcosysteemType relating to the BT 3) area in m2 of (1), 
  4) Provincie to which it is assigned
'''
dat_snl['snl_bt'] = '_'.join(snl for snl in snl_soortlijst)  # beheertypen, possibly > 1
dat_snl['ecosystype'] = clo.bt_to_eco(snl_soortlijst[0])  # stom om alleen [0] te gebruiken, maar goed...


# sort columns neatly (dit moet vast makkelijker kunnen, maar daar ben ik nu te moe voor.)
col_order = ['hok_id', 'snl_bt', 'ecosystype', 'areaal_ha', '1994-2001_vaatplant_SNL', '1994-2001_vaatplant_Bijl1',
             '1994-2001_vogel_SNL', '1994-2001_vogel_Bijl1', '1994-2001_vlinder_SNL', '1994-2001_vlinder_Bijl1',
             '1994-2001_Bijl1_tot', '1994-2001_Bijl1_cap', '1994-2001_SNL_tot', '1994-2001_SNL-Bijl1_tot',
             '1994-2001_CLO1518_score', '2002-2009_vaatplant_SNL', '2002-2009_vaatplant_Bijl1',
             '2002-2009_vogel_SNL', '2002-2009_vogel_Bijl1', '2002-2009_vlinder_SNL', '2002-2009_vlinder_Bijl1',
             '2002-2009_Bijl1_tot', '2002-2009_Bijl1_cap', '2002-2009_SNL_tot', '2002-2009_SNL-Bijl1_tot',
             '2002-2009_CLO1518_score', '2010-2017_vaatplant_SNL', '2010-2017_vogel_SNL', '2010-2017_vogel_Bijl1',
             '2010-2017_vaatplant_Bijl1', '2010-2017_vlinder_SNL', '2010-2017_vlinder_Bijl1', '2010-2017_Bijl1_tot',
             '2010-2017_Bijl1_cap', '2010-2017_SNL_tot', '2010-2017_SNL-Bijl1_tot', '2010-2017_CLO1518_score',
             'provincie', 'twente_hdr']
dat_snl = dat_snl.reindex(columns=col_order)

'''
Write report
'''
timestamp_brief = datetime.datetime.now().strftime("%y%m%d-%H%M")
timestamp_full = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
remaining_snl_area_ha = str(np.float16(dat_snl.loc[:, ['areaal_ha']].sum(axis=0)))

out_dir = r'd:\clo1543\CLO1518\bt_tabel'
basename = 'clo1518_{0}_{1}'.format(snl_soortlijst[0], timestamp_brief)
header = '#Model run dated: {0} by {1}\n#\n#Extract from PGO species distribution data with PGO Query:\n' \
         '#  Soortgroepen: {2}\n#  SNL Soortlijst: {3}\n#  SNL Sub-soortlijst: {4}\n#  ==> {5} observations in {6} ' \
         'different 250m hokken\n#Selection from 250m hokken grid with Beheertypen Query:\n' \
         '#  {7}\n#  ==> {8} 250m hokken with total {9} hectare.\n#Identity between PGO Query and Beheertypen Query ' \
         'leaves:\n#  {10} 250m hokken with {11} hectare.\n#\n'.format(timestamp_full,
                                                                      os.environ.get('USERNAME'),
                                                                      ', '.join([soort for soort in soortgroepen]),
                                                                      ', '.join([snl for snl in snl_soortlijst]),
                                                                      '-'.join([sub for sub in sub_soortlijst]),
                                                                      pgo_dat.shape[0],
                                                                      len(set(pgo_dat.hok_id)),
                                                                      spat_query,
                                                                      hokken.shape[0],
                                                                      str(np.float16(hokken.loc[:, idx[('_', '_', 'areaal_ha')]].sum(axis=0))),
                                                                      dat_snl.shape[0],
                                                                      remaining_snl_area_ha)
footer = '#Made with Python 3.5 using pandas, geopandas, by Hans Roelofsen, WEnR team B&B, ' \
         'dated {0}. For code, see: https://git.wur.nl/roelo008/wenr_clo1543/blob/master/b_analysis/clo_1518_' \
         'analysis.py'.format(timestamp_full)

# TODO: round to integers when writing to file

with open(os.path.join(out_dir, basename + '.txt'), 'w') as f:
    f.write(header)
    f.write(clo1518_1994_2001_report)
    f.write(clo1518_2002_2009_report)
    f.write(clo1518_2010_2017_report)
    f.write(dat_snl.to_csv(sep=';', line_terminator='\r', header=True, index=False))
    f.write(footer)
print('Report to file: {0}'.format(os.path.join(out_dir, basename)))


