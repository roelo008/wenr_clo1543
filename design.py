'''
Ontwerp voor algemene CLO analyse gebasseerd op PGO data

'''

def query_hokken():
    '''
    Geeft lijst van hok_IDs die voldoen aan een query op gebied van
    SNL BT aanwezigheid, Ecosysteemtype aanwezigheid, Provincie, Twente

    :return:
    '''


def query_pgo():
    ''''
    Geeft pandas dataframe van de PGO data gebasseerd op:
      soortgroep
      SNL type soortlijst
      soort sublijst
      periode

    '''

def clo1543():
    '''
    methode for berekening van CLO 1543 indicator

    :return:
    '''


def clo1518():
    '''
    methode voor berekening CLO 1518 indicator

    :return:
    '''


def report():
    '''
    Write report
    :return:
    '''