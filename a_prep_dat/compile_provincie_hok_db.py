'''
Script to assign provincie acreage to the 250m hokken
Each 250m must be assigned to a single provincie
Hokken intersecting with > 1 provincie should be assigned to the provincie with the largest area
Write to output as pickled pandas dataframe for each provincie with 2 cols: area_m2 (always 625000) and hok_id (key)

Hans Roelofsen, okt 2019

'''

import geopandas as gp
import pandas as pd
import pickle
import os
import sys

sys.path.extend(['c:\\apps\\proj_code\\wenr_clo1543', 'c:/apps/proj_code/wenr_clo1543'])

provs = r'c:\Users\roelo008\OneDrive - WageningenUR\b_geodata\provincies_2018\poly\provincies_2018.shp'
hokken = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\geodata\250mhok\shp\hok250m_fullextent.shp'

prov = gp.read_file(provs)
hok = gp.read_file(hokken)

print('1')

# union between provincies and hokken
hok_union = gp.overlay(df1=hok, df2=prov, how='union')

# not all hokken intersect with a provincie (eg North Sea). Fill with 'Geen'
hok_union.provincien.fillna('geen', inplace=True)
# Note that also unions shapes are created with ID == na !!
print('2')
# count how often each hok is represented. Once = entirely wihtin or outside a prov. > 1 means intersecting 2 or more Provs
id_count = pd.pivot_table(hok_union, index='ID', values='provincien', aggfunc='count')
single_id = id_count.loc[id_count['provincien'] == 1, :].index.tolist()
double_id = id_count.loc[id_count['provincien'] > 1, :].index.tolist()

# subset all hokken into those occuring once and occuring twice
singles = hok_union.loc[hok_union.ID.isin(single_id), :]
doubles = hok_union.loc[hok_union.ID.isin(double_id), :]
print('3')
# check if all hokken are accounted for
if len(single_id) + len(double_id) != hok.shape[0]:
    raise Exception('Singles {0} and Doubles {1} do not equal number of hok-unions{2}'.format(len(single_id),
                                                                                              len(double_id),
                                                                                              hok.shape[0]))

# For the doubles, calculate area for each portion in adjoining provincies
doubles.loc[:, 'area'] = doubles.area
doubles_piv = pd.pivot_table(doubles, index='ID', columns='provincien', values='area', aggfunc='sum')
print('4')
# provincie with the maximum areaal :-)
doubles_piv['provincien'] = doubles_piv.idxmax(axis=1, skipna=True)

# format doubles to output format
doubles_piv['area_m2'] = 62500
doubles_piv['hok_id'] = doubles_piv.index
doubles_out = doubles_piv.drop(labels=[lab for lab in list(doubles_piv) if lab not
                                       in ['area_m2', 'hok_id', 'provincien']], axis=1)

# format singles to output format
singles['area_m2'] = 62500
singles.rename(columns={'ID': 'hok_id'}, inplace=True)
singles_out = singles.drop(labels=[lab for lab in list(singles) if lab not in ['area_m2', 'hok_id', 'provincien']],
                           axis=1)

# concatenate
if doubles_out.shape[0] + singles_out.shape[0] == hok.shape[0]:
    df_out = pd.concat([singles_out, doubles_out])
else:
    raise Exception('sinles and doubles do not match input shape of hokken.')

# write to csv per provincie
pkl_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\a_broedvogels\SNL_grids\csv_format'
for prov in list(set(hok_union.provincien)):
    out_name = 'prov-{0}.csv'.format(prov)
    out_df_sel = df_out.loc[df_out.provincien == prov, ['area_m2', 'hok_id']]

    print('Writing {0} records for provincie {1} to pickle at {2}'.format(out_df_sel.shape[0], prov, out_name))
    out_df_sel.to_csv(os.path.join(pkl_dir, out_name), sep=';', index=False)

# write to pickle for all provincies combined
out_name2 = 'hok_ids_provs.pkl'
df_out.to_pickle(os.path.join(pkl_dir, out_name2))
