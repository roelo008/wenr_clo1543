'''
Script to combine *.csv files with PGO data into a HDF5 store. Aim is to create a single HDF5 store from which
  all PGO analysis data can be drawn.

Hans Roelofsen, WEnR team B&B, 10 October 2019
'''

import os
import pandas as pd
import datetime

datestamp = datetime.datetime.now().strftime("%y%m%d-%H%M")
store = pd.HDFStore(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\compiled\hd5\pgo_dat_{0}.h5'.format(datestamp),
                    mode='w')

plant_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\b_vaatplanten\Soortenrijkdom\csv_format'
vogel_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\a_broedvogels\Soortenrijkdom\Species_richness\csv_format'
vlinder_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\c_vlinders'

plant_list = os.listdir(plant_dir)
vogel_list = os.listdir(vogel_dir)

'''Vaatplanten'''
i = 0
for csv in plant_list:
    name, ext = os.path.splitext(csv)
    soortgroep, snl, soortlijst, periode, writetime = name.split('_')

    df = pd.read_csv(os.path.join(plant_dir, csv), sep=';', comment='#')
    print('Reading {0} ({1}/{2})'.format(csv, i, len(plant_list)))

    df['soortgroep'] = soortgroep
    df['snl'] = snl
    df['soortlijst'] = soortlijst
    df['periode'] = periode

    if i == 0:
        store.put(key=soortgroep, value=df, format='t', data_columns=True,
                  min_itemsize={'soortlijst': 11, 'snl': 22, 'soortgroep': 9})
        # Important Note: Querying lukt enkel voor de cols die genoemd izjn in min_itemsize!
        # TODO: minitemsize voor alle kolommen
    else:
        store.append(key=soortgroep, value=df, format='t', data_colums=True)
    print('  ...put to store.')

    i += 1

'''Vogels'''
j = 0
for csv in vogel_list:
    name, ext = os.path.splitext(csv)
    soortgroep, snl, soortlijst, periode, writetime = name.split('_')

    df = pd.read_csv(os.path.join(vogel_dir, csv), sep=';', comment='#')
    print('Reading {0} ({1}/{2})'.format(csv, j, len(vogel_list)))

    df['soortgroep'] = soortgroep
    df['snl'] = snl
    df['soortlijst'] = soortlijst
    df['periode'] = periode

    if j == 0:
        store.put(key=soortgroep, value=df, format='t', data_columns=True,
                  min_itemsize={'soortlijst': 11, 'snl': 22, 'soortgroep': 9})
    else:
        store.append(key=soortgroep, value=df, format='t', data_colums=True)
    print('  ...put to store.')

    j += 1

'''Vlinders'''
vlinder = pd.read_csv(os.path.join(vlinder_dir, 'vlinder_all_v2.txt'), comment='#', sep=';')
store.put(key='vlinder', value=vlinder, format='t', data_colums=True,
          min_itemsize={'soortlijst': 11, 'snl': 22, 'soortgroep': 7})

store.close()