"""
Scrip to generate a pandas dataframe where:
index = 250m hok ID XXXXXX_YYYYYY
columns = fractional cover of Beheertypen (Nxx.yy) and other land cover categories

n features = 1.456.000 (1.120 cols, 1.300 rows)

"""

import os
import pickle
import affine
import numpy as np
import pandas as pd

'''
Create dataframe and basic columns identifying 250m hokken.  
'''
ncols = 1120
nrows = 1300
shape = (nrows, ncols)
rows = np.array([[i] * ncols for i in range(0, nrows)]).reshape(np.product(shape))
cols = np.array([i for i in range(0, ncols)] * nrows).reshape(np.product(shape))

# affine transformation b'teen row-col and RD-New coords for grid
affine_trans = affine.Affine.from_gdal(0, 250, 0, 625000, 0, -250)  # (x topleft, width, 0, y topleft, 0, height)

db = pd.DataFrame({'row': rows, 'col': cols})
db['x_topleft'] = db.apply(lambda x: ((x.col, x.row) * affine_trans)[0], axis=1).astype(np.int32)
db['y_topleft'] = db.apply(lambda x: ((x.col, x.row) * affine_trans)[1], axis=1).astype(np.int32)
db['hok_id'] = db.apply(lambda x: '{0}_{1}'.format(x.x_topleft, x.y_topleft), axis=1)

'''
Add pickled csv versions of the SNL rasters showing fractual cover of LU class X as columns
Never change nrow(db), padd missing cells with 0
'''

pkl_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\a_broedvogels\SNL_grids\csv_format'

for source in [x for x in os.listdir(pkl_dir) if x.endswith('csv') and not x.startswith('prov')]:
    src_name = os.path.splitext(source)[0]
    col_name = src_name.split('_')[0]

    try:
        print('Now adding {0}'.format(src_name))
        source_df = pd.read_csv(os.path.join(pkl_dir, source), sep=';', comment='#')
        source_df.set_index('hok_id', inplace=True, verify_integrity=True)
        source_df.rename(columns={'area_m2': col_name}, inplace=True)

        db = pd.merge(db, source_df, left_on='hok_id', right_index=True, how='left')

        print('  DB shape now: {0} rows, {1} cols'.format(db.shape[0], db.shape[1]))

    except FileNotFoundError:
        print('Warning, file not found: {0}'.format(source))
        continue


db.fillna(0, inplace=True)
intcols = [col for col in list(db) if col not in['row', 'col', 'x_topleft', 'y_topleft', 'hok_id']]
db = db.astype(dict(zip(intcols, ['int32']*len(intcols))))
print(list(db))
print(db.dtypes)
print(db.head())

with open(os.path.join(pkl_dir, 'nl250mgrid_wo-provs.pkl'), 'wb') as handle:
    pickle.dump(db, handle)

# with open(os.path.join(pkl_dir, 'nl250mgrid.pkl'), 'rb') as handle:
#     foo = pickle.load(handle)
