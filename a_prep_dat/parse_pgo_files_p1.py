"""
This script translates all original *.asc from the PGOs and into a flat-table format *.csv file.
All values except for NODATAValues are retained and stored in combination with the cell_id.

The CSV files will be combined into a single hdf5 repository in a subsequent script.

Together, these scripts do essentially the same as https://github.com/haro-nl/pgo_hotspots_WEnR/blob/master/prepare_data/prep_asc.py
but (hopefull) in a better and faster way. If this script works, https://git.wur.nl/roelo008/wenr_clo1543/blob/master/a_prep_dat/prep_pgo_database.py
can also be retired.

Hans Roelofsen, WEnR, team B&B, 8 Oct 2019
"""

import pandas as pd
import os
import numpy as np
import rasterio as rio
import datetime
import sys
import affine

# append path
# sys.path.extend(['D:\\projects_code\\wenr_clo1543', 'D:/projects_code/wenr_clo1543'])

from z_utils import clo

''' 
Create template dataframe with hok_ids for all 1120*1300 250m cellen
'''
ncols = 1120
nrows = 1300
shape = (nrows, ncols)

'''
Create arrays for row and col number:
 row indices range from 0 up to and including specs['NROWS'] -1
 col indices range from 0 up to and including specs['NCOLS'] -1
Although row, col indices are integers (ie 'blocks'), note that row, col (0,0) refers to cell top-left in Cartesian 
space.
Also note that the array iteration is COLS first! I.e. rows = [0, 0, 0 ... 1299, 1299] 
                                                       cols = [0, 1, 2, 3 ... 1117, 1118, 1119]
'''
rows = np.array([[i] * ncols for i in range(0, nrows)]).reshape(np.product(shape))
cols = np.array([i for i in range(0, ncols)] * nrows).reshape(np.product(shape))

'''
Calculate Cartesian (ie RD New coordinates) based on the row, col indices, meaning that they refer to the
cell top-left!!
Affine transformation specs are described here: https://www.perrygeo.com/python-affine-transforms.html as follows:
(x-coord upper-left corner upper-left pixel, pixel-width, 0, y-coord upper-left corner upper-left pixel, 0, pixelheight) 

'''
affine_trans = affine.Affine.from_gdal(0, 250, 0, 625000, 0, -250)  #

df_template = pd.DataFrame({'row': rows, 'col':cols})
df_template['x_topleft'] = df_template.apply(lambda x: ((x.col, x.row) * affine_trans)[0], axis=1).astype(np.int32)
df_template['y_topleft'] = df_template.apply(lambda x: ((x.col, x.row) * affine_trans)[1], axis=1).astype(np.int32)
df_template['hok_id'] = df_template.apply(lambda x: '{0}_{1}'.format(x.x_topleft, x.y_topleft), axis=1)

'''
Run for all ascii files in the in_dir 
'''
in_dir = r'd:\hotspot_working\a_broedvogels\Soortenrijkdom\Species_richness'
out_dir = os.path.join(in_dir, 'csv_format')
key = 'vogel'
i = 0

file_list = [x for x in os.listdir(in_dir) if x.startswith(key) and x.endswith('asc')]
for file in file_list:

    # check if specifications of the file comply to expectations
    specs = clo.get_specs(in_dir, file)
    if specs['NROWS'] != 1300 or specs['NCOLS'] != 1120:
        print('{0} does not comply to expected formatting: '
              '{1}-{2} instead of 1300-1120'.format(file, specs['NROWS'], specs['NCOLS']))
        continue
    else:
        print('Now processing {0} ({1}/{2}) at {3}'.format(file, i, len(file_list),
                                                           datetime.datetime.now().strftime('%H:%M:%S')))

    # read the ascii file
    asc = rio.open(os.path.join(in_dir, file))
    vals = asc.read(1)

    # reshape from NROWS*NCOLS array to 1D vector of same lenght.
    # Note, reshapes over cols first!, so order = 'C'
    # See: https://docs.scipy.org/doc/numpy/reference/generated/numpy.reshape.html
    val_vector = vals.reshape(np.product([specs['NROWS'], specs['NCOLS']]), order='C')

    # append to template to copy hok_ids
    df = pd.DataFrame({'hok_id': df_template['hok_id'], 'n': val_vector})
    df.drop(df.loc[df['n'] == specs['NODATA_value']].index, inplace=True)

    # set to U16
    df['n'] = df['n'].astype(dtype=np.uint16)

    # write to file
    timestamp = datetime.datetime.now().strftime("%y%m%d-%H%M")
    name, ext = os.path.splitext(file)
    out_name = '{0}_{1}.csv'.format(name, timestamp)

    print('  Writing to file with {0} rows\n'.format(df.shape[0]))

    with open(os.path.join(out_dir, out_name), 'w') as f:
        f.write('# {0}, created Hans Roelofsen, {1}\n'.format(name, timestamp))
        f.write(df.to_csv(sep=';', header=True, index=False, line_terminator='\r'))

    i += 1
