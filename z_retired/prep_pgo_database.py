'''
Messing around with hd5 to create a single repository for all PGO data

This script retired as of oct 2019, replaced by `parse_pgo_files_p2.py`

Hans Roelofsen, september 2019

'''

import pandas as pd
import pickle
import datetime

datestamp= datetime.datetime.now().strftime("%y%m%d-%H%M")
store = pd.HDFStore(r'd:\hotspot_working\hd5\pgo_dat_{0}.h5'.format(datestamp))

vogel = pd.read_pickle(r'd:\hotspot_working\a_broedvogels\Soortenrijkdom\Species_richness\vogel_all4.pkl')
store.put('vogel', vogel, format='t', data_columns=True)

vlinder = pd.read_pickle(r'd:\hotspot_working\c_vlinders\vlinder_all_v2.pkl')
store.put('vlinder', vlinder, format='t', data_columns=True)

plant_b1 = pd.read_pickle(r'd:\hotspot_working\b_vaatplanten\Soortenrijkdom\vaatplant_all_Bijl1.pkl')
store.put('plant', plant_b1, format='t', data_columns=True, min_itemsize={'snl':22, 'soortlijst':22})

plant_snl = pd.read_pickle(r'd:\hotspot_working\b_vaatplanten\Soortenrijkdom\vaatplant_all_snl.pkl')
store.append('plant', plant_snl)

plant_ecosys = pd.read_csv(r'd:\hotspot_working\b_vaatplanten\Soortenrijkdom\vaatplant_all_EcoSysLijst.csv',
                           sep=';', comment='#')
store.append('plant', plant_ecosys)

# store.put('vogel', vogel, format='t')
vlinder.to_hdf(r'd:\hotspot_working\hd5\pgo_dat.h5', 'vogel', format='t')