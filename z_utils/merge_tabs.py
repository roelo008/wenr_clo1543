'''
Script to merge two ore more *.csv files into a single file
Merge only lines not starting with '#'
Write source file to end of each line

Hans Roelofsen, 7 nov 2019
'''

import os
import pandas as pd
import datetime

in_dir = r'd:\clo1543\CLO1518\bt_tabel'
in_csv = [csv for csv in os.listdir(in_dir) if csv.endswith('txt')]

out_dir= r'd:\clo1543\CLO1518\totaal_tabel'
out_base_name = 'combined_table_bts'
timestamp_brief = datetime.datetime.now().strftime("%y%m%d-%H%M")

i = 0
holder = []
for csv_file in in_csv:
    df = pd.read_csv(os.path.join(in_dir, csv_file), comment='#', sep=';')
    df['source_file'] = csv_file
    holder.append(df)

df_out = pd.concat(holder)

'''
Write report
'''
timestamp_brief = datetime.datetime.now().strftime("%y%m%d-%H%M")
timestamp_full = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

out_dir = r'd:\clo1543\CLO1518\totaal_tabel'
out_base_name = 'clo1518_totaal_tabel_{0}'.format(timestamp_brief)

header1 = '#Generated {0} by {1}\n'.format(timestamp_full, os.environ.get('USERNAME'))
header2 = '#This file contains the combined content of {0} individual tables. See column "source_file" for the source' \
          'table of each line. The data in this file relate to the PGO Kansenkaarten data for Beheertypen in NL and' \
          'may be used to generate HabitatKwaliteit statistics and/or CLO indicatoren.\n'.format(len(holder))
header3 = '#Please ensure all relevant meatadata for the table below is retained when using this data.\n'
header4 = '#For questions about this data, please ask Bart de Knegt, Marlies Sanders and/or Hans Roelofsen.\n'
footer = '#Made with Python 3.5 using pandas, by Hans Roelofsen, WEnR team B&B, ' \
         'dated {0}. For code, see: ' \
         'https://git.wur.nl/roelo008/wenr_clo1543/blob/master/z_utils/merge_tabs.py'.format(timestamp_full)

# df_out.to_csv(os.path.join(out_dir, '{0}_{1}.txt'.format(out_base_name, timestamp_brief)), index=False, sep=';')
with open(os.path.join(out_dir, out_base_name + '.csv'), 'w') as f:
    f.write(header1)
    f.write(header2)
    f.write(header3)
    f.write(df_out.to_csv(index=False, sep=';', line_terminator='\r',))
    f.write(footer)

