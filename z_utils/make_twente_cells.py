import os
import numpy as np
import pandas as pd
import affine

from z_utils import clo
''' 
Script to generate 250m hokken representing the area of Twente. Note that the source file
twente.pkl was generated manually earlier and is read here from file. See correspondence Marlies Sanders Nov 2019
for details
Hans Roelofsen, WEnR B&B nov 2019

'''

'''
pkl_dir = r'd:\hotspot_working\a_broedvogels\SNL_grids\augurken'
tw = pd.read_pickle(os.path.join(pkl_dir, 'twente.pkl'))

tw['x'] = tw.apply(lambda row: int(row.hok_id[:6]), axis=1)
tw['y'] = tw.apply(lambda row: int(row.hok_id[7:]), axis=1)
'''
# Zie \\wur\dfs-root\PROJECTS\Hotspots_pgos\b_Prelim_Results\250m_grid\Twente\twente_hokken_suggestie_MS.shp
# voor de extent van Twente
left = 241500
right = 269250
bottom = 459250
top = 485500

ncols = np.int64(np.sum([np.divide(np.subtract(right, left), 250), 1]))
nrows = np.int64(np.sum([np.divide(np.subtract(top, bottom), 250), 1]))

shape = (nrows, ncols)
rows = np.array([[i] * ncols for i in range(0, nrows)]).reshape(np.product(shape))
cols = np.array([i for i in range(0, np.int64(ncols))] * nrows).reshape(np.product(shape))

# affine transformation b'teen row-col and RD-New coords for grid
affine_trans = affine.Affine.from_gdal(left, 250, 0, top, 0, -250)  # (x topleft, width, 0, y topleft, 0, height)

db = pd.DataFrame({'row': rows, 'col': cols})
db['x_topleft'] = db.apply(lambda x: ((x.col, x.row) * affine_trans)[0], axis=1).astype(np.int32)
db['y_topleft'] = db.apply(lambda x: ((x.col, x.row) * affine_trans)[1], axis=1).astype(np.int32)
db['hok_id'] = db.apply(lambda x: '{0}_{1}'.format(x.x_topleft, x.y_topleft), axis=1)

db['area_m2'] = 62500
pkl_dir = r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\a_broedvogels\SNL_grids\csv_format'
db.drop(labels=['row', 'col', 'x_topleft', 'y_topleft'], axis=1).to_csv(os.path.join(pkl_dir, 'twente_hdr.csv'),
                                                                        sep=';', index=False)
clo.hok_id_df_to_shp(db, 'twente_suggestie_hdr')