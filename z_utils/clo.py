'''
Helper functions for various CLO indicators based on PGO kansenkaarten.

Hans Roelofsen, sept 2019

'''

import os
import pandas as pd
import geopandas as gp
import numpy as np
import random
import string


def classifier(x, bins, labels):
    '''
    Returns label corresponding to the bin in which x slots

    :param x: the value to be slotted into one of the bins
    :param bins: bin edges, preferably from np.histogram
    :param labels: list of labels corresponding to each bin
    :return: appropriate label
    '''

    if x in range(bins[0], bins[-1]):

        bin_nr = np.digitize(x, bins, right=False)  # see: https://docs.scipy.org/doc/numpy/reference/generated/numpy.digitize.html
        return labels[bin_nr-1]  # because digitze returns 1 for first bin, not 0
    else:
        print('{0} cannot be classified into a category, outside bin range: {1}'.format(x, bins))
        return np.nan

'''
def classifier(x, categories, labels):
    # returns corresponding label for numerical category containing x
    try:
        return labels[[np.int32(x) in cat_range for cat_range in categories].index(True)]
    except ValueError:
        return np.nan
'''


def df_2_gdf(df):
    '''

    :param df: pandas dataframe with column hok_id
    :return: df as geodataframe with column geometry attached
    '''

    if not isinstance(df, pd.core.frame.DataFrame):
        raise Exception('I only accept panda dataframes')
    if 'hok_id' not in list(df):
        raise Exception('No column named hok_id')

    # full extent shp
    try:
        nlgrid = gp.read_file(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\geodata\250mhok\shp\hok250m_fullextent.shp')
    except OSError:
        raise Exception('Requesting file from laptop Hans Roelofsen. A copy is stored here'
                        ': wur\dfs-root/PROJECTS/hotspots_pgos/b_Prelim_Results/250_grid_shp/hok250m_fullextent.shp')

    out_df = pd.merge(left=df, right=nlgrid.loc[:, ['ID', 'geometry']], left_on='hok_id', right_on='ID',
                      how='left')
    out_gdf = gp.GeoDataFrame(out_df, geometry='geometry')
    return out_gdf


def hok_id_df_to_shp(df, out_dir, out_name):
    '''
    :param df: pandas dataframe with column hok_id
           out_name: base name of output shapefile
    :return: ply shapefile with all unique hok_ids
    '''

    if not type(df) == pd.core.frame.DataFrame:
        raise Exception('I only accept panda dataframes')
    if 'hok_id' not in list(df):
        raise Exception('No column named hok_id')

    hokken = set(df.hok_id)
    # full extent shp
    try:
        nlgrid = gp.read_file(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\geodata\250mhok\shp\hok250m_fullextent.shp')
    except OSError:
        raise Exception('Requesting file from laptop Hans Roelofsen. A copy is stored here'
                        ': wur\dfs-root/PROJECTS/hotspots_pgos/b_Prelim_Results/250_grid_shp/hok250m_fullextent.shp')

    sel_grid = nlgrid.loc[nlgrid['ID'].isin(hokken)]
    sel_grid.to_file(os.path.join(out_dir, out_name + '.shp'))



def get_specs(dir_in, asc_in):
    # Return specs of ASC grid file as a dictionary
    specs = {}
    with open(os.path.join(dir_in, asc_in), 'r') as f:
        for line in f.readlines():
            if not line.startswith('-9999'):
                specs[line.split(' ')[0]] = line.split(' ')[1]
    for k,v in specs.items():
        specs[k] = np.int32(np.float32(v))
    return specs


def ran_gen(size, chars=string.ascii_uppercase + string.digits):
    '''
    generate random ascii string as model run identifier
    '''
    return ''.join(random.choice(chars) for x in range(size))


def bt_to_eco(bt):
    bt_2_eco = {
     'N0501': 'Moeras',
     'N0502': 'Moeras',
     'N0601': 'Moeras',
     'N0602': 'Moeras',
     'N0603': 'Heide',
     'N0604': 'Heide',
     'N0605': 'Heide',
     'N0606': 'Heide',
     'N0701': 'Heide',
     'N0702': 'Heide',
     'N0801': 'OpenDuin',
     'N0802': 'OpenDuin',
     'N0803': 'OpenDuin',
     'N0804': 'OpenDuin',
     'N0901': 'HalfnatuurlijkGrasland',
     'N1001': 'HalfnatuurlijkGrasland',
     'N1002': 'HalfnatuurlijkGrasland',
     'N1101': 'HalfnatuurlijkGrasland',
     'N1201': 'HalfnatuurlijkGrasland',
     'N1202': 'HalfnatuurlijkGrasland',
     'N1203': 'HalfnatuurlijkGrasland',
     'N1204': 'HalfnatuurlijkGrasland',
     'N1205': 'HalfnatuurlijkGrasland',
     'N1206': 'HalfnatuurlijkGrasland',
     'N1301': 'HalfnatuurlijkGrasland',
     'N1302': 'HalfnatuurlijkGrasland',
     'N1401': 'Bos',
     'N1402': 'Bos',
     'N1403': 'Bos',
     'N1501': 'Bos',
     'N1502': 'Bos',
     'N1601': 'Bos',
     'N1602': 'Bos',
     'N1603': 'Bos',
     'N1604': 'Bos',
     'N1701': 'Bos',
     'N1702': 'Bos',
     'N1703': 'Bos',
     'N1704': 'Bos',
     'N1705': 'Bos',
     'N1706': 'Bos'}

    try:
        return bt_2_eco[bt]
    except KeyError:
        return 'onbekend'


def query_250m_hokken(snl_wel, th, snl_niet):
    '''
    Returns pd dataframe with selected 250m hokken as rows and area in sq m2 of various SNL BeheerTypes, Provincies, etc
     as columns. Column areaal gives area in hectare of all columns provided in 'snl_wel'
    :param snl_wel: SNL BeheerTypes, Provinicies or other where sq meter > 0. Must be in columns
    :param th: treshold in m2 for snl types WEL
    :param snl_niet: idem, where sq m == 0
    :return: dataframe
    '''

    # compile the spatial query
    spatial_query_p1 = ' | '.join(["{0} > {1}".format(x, th) for x in snl_wel])  # cells must have > 0 m2 of each snl
    spatial_query_p2 = ' & '.join(["{0} == 0".format(x) for x in snl_niet])  # cells must have 0 m2 of each snl
    spatial_query = ' & '.join(['({0})'.format(x) for x in [spatial_query_p1, spatial_query_p2] if len(x) > 0])

    nl250 = pd.read_pickle(os.path.join(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\geodata\pkl',
                                        'nl250mgrid_wo-provs.pkl'))
    hokken = nl250.query(spatial_query, engine='python')  # reduce to hokken complying to the spatial query
    if hokken.empty:
        raise Exception('No hokken found complying to query.')
    else:
        print('Querying {0} from nl250 hokken geeft {1} 250m hokken.'.format(spatial_query, hokken.shape[0]))

    hokken.set_index('hok_id', inplace=True, drop=False)  # set index
    hokken['areaal_m2'] = hokken.loc[:, snl_wel].sum(axis=1)  # calculate areaal per hok of selected SNL types
    hokken['areaal_ha'] = hokken.apply(lambda row: np.divide(row.areaal_m2, 10000), axis=1)  # sq m to hectare
    try:
        hokken['provincie'] = hokken.filter(regex='^prov', axis='columns').idxmax(axis=1)  # name of provincie for each hok
    except ValueError:
        pass
    # add 2 additional columns levels to allow merging with pgo_piv later on

    return hokken, spatial_query


def query_pgo_store(query):
    '''
    Function to query HD5 store of PGO data.

    :param query: query on PGO columns SNL, soortlijst and/or soortgroep
    :return: concatenated pd dataframe
    '''
    # open store and read datasets for vogel, plant, vlinder
    store = pd.HDFStore(r'c:\Users\roelo008\OneDrive - WageningenUR\a_projects\PGO_Hotspots\compiled\hd5\pgo_dat_191204-1537.h5', mode='r')
    print('Querying PGO store with {0}'.format(query))
    vogel = store.select(key='vogel', where=query)
    plant = store.select(key='vaatplant', where=query)
    vlinder = store.select(key='vlinder', where=query)
    store.close()

    # check and report
    if all([vogel.empty, plant.empty, vlinder.empty]):
        raise Exception('No suitable data found')
    else:
        print('Found {0} vogels, {1} planten and {2} vlinders'.format(vogel.shape[0], plant.shape[0], vlinder.shape[0]))
        return pd.concat([vogel, plant, vlinder], sort=False)  # TODO: harmonize columns?


def calc_clo_1518(df, periode, labs):
    idx = pd.IndexSlice
    try:
        hok_tot_species = df.loc[:, idx[periode, 'SNL-Bijl1', 'tot']]  # Series with total species count
        max_species_hok = hok_tot_species.idxmax()  # index (ie hok_id) of the highest value
        max_species = hok_tot_species[max_species_hok]  # max nr of species for this BT
        _, bins = np.histogram(np.arange(np.sum([max_species, 1])), 4)  # 4 bin histogram between 0 and max_species
        clo_score = hok_tot_species.apply(classifier, bins=bins, labels=labs)  # classify tot nr species of each cell
        # into one of 4 quality scores

        # format report
        bin_list = bins.tolist()
        bins_pretty_print = ['{0} <= n < {1}'.format(bin_list[i - 1], bin_list[i]) for i in
                             range(1, len(bin_list) - 1)] + \
                            ['{0} <= n <= {1}'.format(bin_list[-2], bin_list[-1])]
        bin_report = '#The cell with highest nr of species for periode {0} is {1} with {2} species (see column {3}). ' \
                     'For column "{0}_CLO1518_score" the scores are as follows:\n#  Laag: {4}\n#  Vrij Laag: {5}\n#  ' \
                     'Vrij Hoog: {6}\n#  ' \
                     'Hoog: {7}\n#\n'.format(periode, max_species_hok, max_species,
                                             '{0}_SNL-Bijl1_tot'.format(periode), bins_pretty_print[0],
                                             bins_pretty_print[1], bins_pretty_print[2], bins_pretty_print[3])

        return clo_score.rename('{0}_CLO1518_score'.format(periode)), bin_report

    except KeyError as e:
        print('Failed to calculate CLO1518')
        print(e)


def calc_clo_1543(s_periode_A, s_periode_B, bins, labels, species):
    """Function to calculate CLOR1543 score, i.e. trend in species number and corresponding score
    s_periode_A: pd Series with species count prior periode
    s_periode_B: pd Series with species count posterior periode (calculation is: periodeA - periodeB
    bins: categories containing the species difference
    labels: labels to identify categories
    returns: DataFrame of differences and score-labels with same index as df
    """

    try:
        sp_diff = s_periode_A.sub(s_periode_B)
        sp_diff.name = 'trend_{0}'.format(species)
        sp_diff_score = sp_diff.apply(classifier, bins=bins, labels=labels)
        sp_diff_score.name = 'score_{0}'.format(species)

        return sp_diff, sp_diff_score

    except KeyError as e:
        print('Failed to calculate CLO1543')
        print(e)