# wenr_clo1543

code for update CLO indicator 1543: https://www.clo.nl/indicatoren/nl1543-ontwikkeling-soorten-natuurgebied-en-agrarisch-gebied
. Gebasseerd op de PGO data: \\wur\dfs-root\PROJECTS\hotspots_pgos\a_Source_Data\ 

#####Periodes
* p1: 1994-2001
* p2: 2002-2009
* p3: 2010-2017

####Trends
* afname: 2 of nog minder 
* stabiel: -1, 0 of 1
* toename 2 of nog meer

#####Voor natuurgebieden:
* selecteer 250m grid cellen with area beheertype N12.02 en N13.02 > 0.
* bepaal areaal per cel voor N12.02 en N13.02
* N13.01 aantal vogels (SNL + Bijlage 1) en aantal vlinders
* N12.02 aantal planten (SNL + Bijlage 1) 
* sommeer aantal soorten en bereken trend (afname, stabiel, toename) voor p2-p3
* update totaal areaal afname-stabiel-toename

#####Voor agrarisch gebied
* idem, maar selecteer gridcellen zonder N-typen en met Top10NL agrarisch grasland
* gebasseerd op Top10NL 2.5 m raster, BNL code 100 

 